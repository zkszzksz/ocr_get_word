package com.nd.orc_get_word.db;

import com.nd.ocr_get_word.entity.OcrWord;
import com.nd.ocr_get_word.util.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: HandleGameinfoDB.java
 * @Package com.inwhoop.gameproduct.db
 * @Description: TODO 操作游戏信息
 * @date 2014-4-16 下午3:10:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class HandleOcrWordDB {
    private OcrWordsDatabaseDBhelper dbhelper = null;

    private SQLiteDatabase readDB, writeDB;
    
	public HandleOcrWordDB(Context context) {
		dbhelper =OcrWordsDatabaseDBhelper.getInstance(context);
	}

	/**
	 * 
	 * 根据关键字查询词语
	 * 
	 * @Title: getOcrWord 
	 * @Description: TODO
	 * @param @param word
	 * @param @return     
	 * @return OcrWord
	 */
	public OcrWord getOcrWord(String word) {
		OcrWord ocrWord = null;
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB.rawQuery("select * from t_word where word=?", new String[]{""+word});
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					ocrWord = new OcrWord();
					ocrWord.id = cursor
							.getInt(cursor.getColumnIndex("id"));
					ocrWord.word = cursor.getString(cursor
							.getColumnIndex("word"));
					ocrWord.explain = cursor.getString(cursor
							.getColumnIndex("explain"));
					if(Utils.strIsEnglish(word)){
						ocrWord.isEng = true;
					}else{
						ocrWord.isEng = false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return ocrWord;
	}
	
}
