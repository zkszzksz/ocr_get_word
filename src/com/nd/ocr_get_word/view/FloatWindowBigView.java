package com.nd.ocr_get_word.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.method.TextKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.iflytek.speech.SpeechError;
import com.iflytek.speech.SynthesizerPlayer;
import com.iflytek.speech.SynthesizerPlayerListener;
import com.nd.ocr_get_word.R;
import com.nd.ocr_get_word.util.ShowWindowManager;


/**
 * @author张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @Description: 弹出的大窗口，用于设置
 * @Package com.nd.ocr_get_word.view
 * @Date: 2014/11/19 14:05
 * @version: 1.0
 */
public class FloatWindowBigView extends LinearLayout implements SynthesizerPlayerListener {

    /**
     * 记录大悬浮窗的宽度
     */
    public static int viewWidth;

    /**
     * 记录大悬浮窗的高度
     */
    public static int viewHeight;
    private SynthesizerPlayer mSynthesizerPlayer;
    private SharedPreferences mSharedPreferences;
    private TextView mSourceText;

    public FloatWindowBigView(final Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.float_window_big, this);
        View view = findViewById(R.id.big_window_layout);

        mSourceText = (TextView) findViewById(R.id.tv_top_text);
//        mSourceText.setText("咖啡厅里买咖啡\nEnglish is a West Germanic language that was first spoken in early medieval England");
        mSourceText.setKeyListener(TextKeyListener.getInstance());
        mSourceText.setInputType(EditorInfo.TYPE_CLASS_TEXT | EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE);

        viewWidth = view.getLayoutParams().width;
        viewHeight = view.getLayoutParams().height;
        ImageView voice = (ImageView) findViewById(R.id.close);
        Button back = (Button) findViewById(R.id.back);
        voice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                synthetizeInSilence(context);
            }
        });
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击返回的时候，移除大悬浮窗，创建小悬浮窗  
                ShowWindowManager.removeBigWindow(context);
                ShowWindowManager.createSmallWindow(context);
            }
        });
        mSharedPreferences = context.getSharedPreferences(context.getPackageName(),
                context.MODE_PRIVATE);
    }

//    @Override
//    protected void onStop() {
//        if (null != mSynthesizerPlayer) {
//            mSynthesizerPlayer.cancel();
//        }
//        super.onStop();
//    }

    private void synthetizeInSilence(Context context) {
        if (null == mSynthesizerPlayer) {
            mSynthesizerPlayer = SynthesizerPlayer.createSynthesizerPlayer(
                    context, "appid=" + context.getString(R.string.app_id));
        }

        String role = mSharedPreferences.getString(
                context.getString(R.string.preference_key_tts_role),
                context.getString(R.string.preference_default_tts_role));
        mSynthesizerPlayer.setVoiceName(role);

        int speed = mSharedPreferences.getInt(
                context.getString(R.string.preference_key_tts_speed),
                50);
        mSynthesizerPlayer.setSpeed(speed);

        int volume = mSharedPreferences.getInt(
                context.getString(R.string.preference_key_tts_volume),
                50);
        mSynthesizerPlayer.setVolume(volume);

        String music = mSharedPreferences.getString(
                context.getString(R.string.preference_key_tts_music),
                context.getString(R.string.preference_default_tts_music));
        mSynthesizerPlayer.setBackgroundSound(music);


        String source = mSourceText.getText().toString();
        source = ""+source.substring(source.indexOf(" ")+1, source.length());
        if ("".equals(source)) {
            Toast.makeText(context, "无内容", 3000).show();
            return;
        }

        mSynthesizerPlayer.playText(source, null, this);
//        mToast.setText(String
//                .format(context.getString(R.string.tts_toast_format), 0, 0));
//        mToast.show();
    }

    @Override
    public void onBufferPercent(int percent, int beginPos, int endPos) {
//        mPercentForBuffering = percent;
//        mToast.setText(String.format(getString(R.string.tts_toast_format),
//                mPercentForBuffering, mPercentForPlaying));
//        mToast.show();
    }

    @Override
    public void onPlayBegin() {
    }

    @Override
    public void onPlayPaused() {
    }

    @Override
    public void onPlayPercent(int percent, int beginPos, int endPos) {
//        mPercentForPlaying = percent;
//        mToast.setText(String.format(getString(R.string.tts_toast_format),
//                mPercentForBuffering, mPercentForPlaying));
//        mToast.show();
    }

    @Override
    public void onPlayResumed() {
    }

    @Override
    public void onEnd(SpeechError arg0) {
    }
}