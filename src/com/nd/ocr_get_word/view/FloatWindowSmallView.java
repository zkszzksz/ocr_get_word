package com.nd.ocr_get_word.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.nd.ocr_get_word.R;
import com.nd.ocr_get_word.util.LogUtil;
import com.nd.ocr_get_word.util.ShowWindowManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

/**
 * @author张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @Description: 可拖动小窗口，用于取词确定
 * @Package com.nd.ocr_get_word.view
 * @Date: 2014/11/19 14:05
 * @version: 1.0
 */
public class FloatWindowSmallView extends LinearLayout {

	/**
	 * 记录小悬浮窗的宽度
	 */
	public static int viewWidth;

	/**
	 * 记录小悬浮窗的高度
	 */
	public static int viewHeight;

	private int vieww = 0;
	private int viewh = 0;

	/**
	 * 记录系统状态栏的高度
	 */
	private static int statusBarHeight;

	/**
	 * 用于更新小悬浮窗的位置
	 */
	private WindowManager windowManager;

	/**
	 * 小悬浮窗的参数
	 */
	private WindowManager.LayoutParams mParams;

	/**
	 * 记录当前手指位置在屏幕上的横坐标值
	 */
	private float xInScreen;

	/**
	 * 记录当前手指位置在屏幕上的纵坐标值
	 */
	private float yInScreen;

	/**
	 * 记录手指按下时在屏幕上的横坐标的值
	 */
	private float xDownInScreen;

	/**
	 * 记录手指按下时在屏幕上的纵坐标的值
	 */
	private float yDownInScreen;

	/**
	 * 记录手指按下时在小悬浮窗的View上的横坐标的值
	 */
	private float xInView;

	/**
	 * 记录手指按下时在小悬浮窗的View上的纵坐标的值
	 */
	private float yInView;
	private long downTime; // 按下时时间

	private String bigPath = Environment.getExternalStorageDirectory()
			+ "/OCRimg.jpeg";

	private float nowx, nowy;

	/**
	 * 得到截图？
	 * 
	 * @return
	 */
	public static Bitmap getScreenBmp() {
		return screenBmp;
	}

	private static Bitmap screenBmp = null;

	// private MyView myView;

	public FloatWindowSmallView(Context context) {
		super(context);
		windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		LayoutInflater.from(context).inflate(R.layout.float_window_small, this);
		View view = findViewById(R.id.small_window_layout);
		viewWidth = view.getLayoutParams().width;
		viewHeight = view.getLayoutParams().height;
		// myView = new MyView(context);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		vieww = getWidth();
		viewh = getHeight();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// 手指按下时记录必要数据,纵坐标的值都需要减去状态栏高度
			xInView = event.getX();
			yInView = event.getY();
			xDownInScreen = event.getRawX();
			yDownInScreen = event.getRawY() - getStatusBarHeight();
			// yDownInScreen = event.getRawY();
			xInScreen = event.getRawX();
			yInScreen = event.getRawY() - getStatusBarHeight();
			// yInScreen = event.getRawY();

			downTime = System.currentTimeMillis();
			break;
		case MotionEvent.ACTION_MOVE:
			xInScreen = event.getRawX();
			yInScreen = event.getRawY() - getStatusBarHeight();
			// 手指移动的时候更新小悬浮窗的位置
			updateViewPosition();
			break;
		case MotionEvent.ACTION_UP:
			long end_time = System.currentTimeMillis();
			double arg1 = (end_time - downTime) / 1000.00; // 秒
			// 如果手指离开屏幕时，xDownInScreen和xInScreen相等，且yDownInScreen和yInScreen相等，则视为触发了单击事件。
			if (xDownInScreen == xInScreen && yDownInScreen == yInScreen) {
				if (arg1 >= 0.32)
					openMainActivity();
				else
					nowx = event.getX();
				nowy = event.getY();
				yInScreen = yInScreen + getStatusBarHeight();
				yInScreen = yInScreen - nowy;
				xInScreen = xInScreen - nowx + vieww / (float) 4;
				openBigWindow();
			}
			break;
		default:
			break;
		}
		return true;
	}

	// 启动主界面
	private void openMainActivity() {
		try {
			Intent intent = getContext().getPackageManager()
					.getLaunchIntentForPackage("com.nd.ocr_get_word");
			getContext().startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将小悬浮窗的参数传入，用于更新小悬浮窗的位置。
	 * 
	 * @param params
	 *            小悬浮窗的参数
	 */
	public void setParams(WindowManager.LayoutParams params) {
		mParams = params;
	}

	/**
	 * 更新小悬浮窗在屏幕中的位置。
	 */
	private void updateViewPosition() {
		mParams.x = (int) (xInScreen - xInView);
		mParams.y = (int) (yInScreen - yInView);
		windowManager.updateViewLayout(this, mParams);
	}

	/**
	 * 打开大悬浮窗，同时关闭小悬浮窗。
	 */
	private void openBigWindow() {
		this.setVisibility(INVISIBLE);
		handler.sendEmptyMessageDelayed(0, 100);
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			screenshot();
        }
	};

	/**
	 * 用于获取状态栏的高度。
	 * 
	 * @return 返回状态栏高度的像素值。
	 */
	private int getStatusBarHeight() {
		if (statusBarHeight == 0) {
			try {
				Class<?> c = Class.forName("com.android.internal.R$dimen");
				Object o = c.newInstance();
				Field field = c.getField("status_bar_height");
				int x = (Integer) field.get(o);
				statusBarHeight = getResources().getDimensionPixelSize(x);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return statusBarHeight;
	}

	/**
	 * 小窗口在屏幕上的x坐标
	 * 
	 * @return
	 */
	public float getxInView() {
		return xInView;
	}

	/**
	 * 小窗口在屏幕上的y坐标
	 * 
	 * @return
	 */
	public float getyInView() {
		return yInView;
	}

	/**
	 * 截屏 ,保存图
	 * 
	 * @return
	 */
	public void screenshot() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg=new Message();
                try {
                    // 截屏
                    Process sh = Runtime.getRuntime().exec("su", null, null);
                    OutputStream os = sh.getOutputStream();
                    os.write(("/system/bin/screencap -p " + bigPath).getBytes("ASCII"));
                    os.flush();
                    os.close();
                    sh.waitFor();
                    LogUtil.i("screenshot,over.bigPath:"+bigPath);
                    msg.what=1;
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.printf("screenshot,excation:" + e.toString());
                    LogUtil.e("screenshot,excation:" + e.toString());
                    msg.what=0;
                }finally {
                    cutHandler.sendMessage(msg);
                }
            }
        }).start();
    }

    private Handler cutHandler =new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    cutAndShowBigWindow();
                    break;
                case 0:
                    Toast.makeText(getContext(),"读取失败",Toast.LENGTH_SHORT).show();
                    ShowWindowManager.createSmallWindow(getContext());
                    break;
            }
        }
    };

    private void cutAndShowBigWindow() {
        getCutBitmap(200, 100);
        ShowWindowManager.createBigWindow(getContext());
        ShowWindowManager.removeSmallWindow(getContext());
    }

    public void getCutBitmap(int cut_w, int cut_h) {
		try {
			Bitmap temp = null;
			File file = new File(bigPath);
//			if (file != null && file.isFile()) {
//				if (temp == null)
					temp = BitmapFactory.decodeFile(bigPath);
//			}
			Matrix matrix = new Matrix();
//			matrix.postRotate(180);  //zk的酷派平板需要把屏幕图片旋转180度
			temp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(),
					temp.getHeight(), matrix, true);
			int width = 0;
			int height = 0;
			float scale = 0;
			if (((int) xInScreen + 80) > temp.getWidth()) {
				width = temp.getWidth() - (int) xInScreen + 80-1;
				scale = 80 / (float) width;
				xInScreen = xInScreen - 80;
			} else {
				if ((xInScreen - 80) < 0) {
					width = (int) xInScreen + 80;
					scale = ((float)xInScreen) / ((float) width);
					xInScreen = 0;
				} else {
					xInScreen = xInScreen - 80;
					width = 160;
					scale = (float) 0.5;
				}
			}
			if ((yInScreen - 35) < 0) {
				yInScreen = 0;
			} else {
				yInScreen = yInScreen - 35;
			}
			height = 40;
			screenBmp = Bitmap.createBitmap(temp, (int) xInScreen,
					(int) yInScreen, width, height);
//			screenBmp = convertGreyImg(screenBmp);
			screenBmp = getCenterletter(screenBmp);
			getALLLetterBitmap(screenBmp, scale);
			screenBmp = getWidthBitmap(screenBmp, scale);
			FileOutputStream fout = new FileOutputStream(
					Environment.getExternalStorageDirectory() + "/OCRimg2.jpeg");
			screenBmp.compress(Bitmap.CompressFormat.JPEG, 100, fout);
			// screenBmp = sharpenImageAmeliorate(screenBmp);
		} catch (Exception e) {
			System.out.println("=============createBitmap=====ee======"
					+ e.toString());
			e.printStackTrace();
		}
	}

	private Bitmap getCenterletter(Bitmap bitmap) throws FileNotFoundException {
		int width = bitmap.getWidth(); // 获取位图的宽
		int height = bitmap.getHeight();
		int[] pix = new int[width * height];
		bitmap.getPixels(pix, 0, width, 0, 0, width, height);
		int downy = 0;
		int upy = 0;
		int mid = height / 4 * 3;
		for (int i = mid; i < height; i++) {
			boolean flag = false;
			for (int j = 0; j < width - 2; j++) {
				if (pix[width * i + j] != pix[width * i + j + 1]
						&& pix[width * i + j] != pix[width * i + j + 2]) {
					if (compareColor(pix[width * i + j], pix[width * i + j + 2])) {
						j++;
					} else {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				downy = i+1;
				break;
			}
		}
		if (downy == 0||downy>height) {
			downy = height;
		}
		for (int i = mid; i >= 0; i--) {
			boolean flag = false;
			for (int j = 0; j < width - 2; j++) {
				if (pix[width * i + j] != pix[width * i + j + 1]
						&& pix[width * i + j] != pix[width * i + j + 2]) {
					if (compareColor(pix[width * i + j], pix[width * i + j + 2])) {
						j++;
					} else {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				upy = i-1;
				break;
			}
		}
		if(upy<0){
			upy = 0;
		}
		bitmap = Bitmap.createBitmap(bitmap, 0, upy, width, downy - upy);
		return bitmap;
	}

	private Bitmap getWidthBitmap(Bitmap bitmap, float scale) {
		int width = bitmap.getWidth(); // 获取位图的宽
		int height = bitmap.getHeight();
		int[] pix = new int[width * height];
		bitmap.getPixels(pix, 0, width, 0, 0, width, height);
		int leftx = 0;
		int rightx = 0;
		int mid = (int) (width * scale);
		for (int i = mid; i < width; i++) {
			boolean flag = false;
			for (int j = 0; j < height - 2; j++) {
				if (pix[width * j + i] != pix[width * (j + 1) + i]
						&& pix[width * j + i] != pix[width * (j + 2) + i]) {
					if (compareColor(pix[width * j + i], pix[width * (j + 2)
							+ i])) {
						j++;
					} else {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				rightx = i+1;
				break;
			}
		}
		if (rightx == 0||rightx>width) {
			rightx = width;
		}
		for (int i = mid; i >= 0; i--) {
			boolean flag = false;
			for (int j = 0; j < height - 2; j++) {
				if (pix[width * j + i] != pix[width * (j + 1) + i]
						&& pix[width * j + i] != pix[width * (j + 2) + i]) {
					if (compareColor(pix[width * j + i], pix[width * (j + 2)
							+ i])) {
						j++;
					} else {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				leftx = i;
				break;
			}
		}
		bitmap = Bitmap.createBitmap(bitmap, leftx, 0, rightx - leftx, height);
		return bitmap;
	}

	private Bitmap getALLLetterBitmap(Bitmap bitmap, float scale)
			throws FileNotFoundException {
		int width = bitmap.getWidth(); // 获取位图的宽
		int height = bitmap.getHeight();
		int[] pix = new int[width * height];
		bitmap.getPixels(pix, 0, width, 0, 0, width, height);
		int leftx = 0;
		int rightx = 0;
		int mid = (int) (width * scale);
		int rightcount = 0;
		for (int i = mid; i < width; i++) {
			boolean flag = false;
			for (int j = 0; j < height - 2; j++) {
				if (pix[width * j + i] != pix[width * (j + 1) + i]
						&& pix[width * j + i] != pix[width * (j + 2) + i]) {
					if (compareColor(pix[width * j + i], pix[width * (j + 2)
							+ i])) {
						j++;
					} else {
						rightcount = 0;
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				rightcount++;
				rightx = i+1;
				if (rightcount == 3) {
					break;
				}
			}
		}
		if (rightx == 0||rightx>width) {
			rightx = width;
		}
		int leftcount = 0;
		for (int i = mid; i >= 0; i--) {
			boolean flag = false;
			for (int j = 0; j < height - 2; j++) {
				if (pix[width * j + i] != pix[width * (j + 1) + i]
						&& pix[width * j + i] != pix[width * (j + 2) + i]) {
					if (compareColor(pix[width * j + i], pix[width * (j + 2)
							+ i])) {
						j++;
					} else {
						leftcount = 0;
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				leftcount++;
				leftx = i;
				if (leftcount == 3) {
					break;
				}
			}
		}
		if(leftx<0){
			leftx = 0;
		}
		Bitmap allbitmap = Bitmap.createBitmap(bitmap, leftx, 0,
				rightx - leftx, height);
		FileOutputStream fout = new FileOutputStream(
				Environment.getExternalStorageDirectory() + "/OCRimg3.jpeg");
		allbitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
		return bitmap;
	}

	private boolean compareColor(int px1, int px2) {
		int r1 = (px1 >> 16) & 0xff;
		int g1 = (px1 >> 8) & 0xff;
		int b1 = px1 & 0xff;

		int r = (px2 >> 16) & 0xff;
		int g = (px2 >> 8) & 0xff;
		int b = px2 & 0xff;

		int dr = Math.abs(r1 - r);
		int dg = Math.abs(g1 - g);
		int db = Math.abs(b1 - b);
		if (dr < 20 && dg < 20 && db < 20) {
			return true;
		}
		return false;
	}

	/**
	 * 将彩色图转换为灰度图
	 * 
	 * @param img
	 *            位图
	 * @return 返回转换好的位图
	 */
	public Bitmap convertGreyImg(Bitmap img) {
		int width = img.getWidth(); // 获取位图的宽
		int height = img.getHeight(); // 获取位图的高

		int[] pixels = new int[width * height]; // 通过位图的大小创建像素点数组

		img.getPixels(pixels, 0, width, 0, 0, width, height);
		int alpha = 0xFF << 24;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int grey = pixels[width * i + j];

				int red = ((grey & 0x00FF0000) >> 16);
				int green = ((grey & 0x0000FF00) >> 8);
				int blue = (grey & 0x000000FF);

				grey = (int) ((float) red * 0.3 + (float) green * 0.59 + (float) blue * 0.11);
				grey = alpha | (grey << 16) | (grey << 8) | grey;
				pixels[width * i + j] = grey;
			}
		}
		Bitmap result = Bitmap.createBitmap(width, height, Config.RGB_565);
		result.setPixels(pixels, 0, width, 0, 0, width, height);
		return result;
	}

	/**
	 * 图像黑白化
	 * 
	 * @param bmp
	 * @return
	 */
	private Bitmap sharpenImageGary(Bitmap bitmap) {
		long start = System.currentTimeMillis();
		int height = bitmap.getHeight();
		int width = bitmap.getWidth();
		int[] pix = new int[width * height];
		bitmap.getPixels(pix, 0, width, 0, 0, width, height);
		int R, G, B;
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {
				int index = y * width + x;
				int r = (pix[index] >> 16) & 0xff;
				int g = (pix[index] >> 8) & 0xff;
				int b = pix[index] & 0xff;
				int s = (r + g + b) / 3;
				// 图像黑白化
				R = s;
				R = (R < 0) ? 0 : ((R > 255) ? 255 : R);
				G = s;
				G = (G < 0) ? 0 : ((G > 255) ? 255 : G);
				B = s;
				B = (B < 0) ? 0 : ((B > 255) ? 255 : B);
				pix[index] = 0xff000000 | (R << 16) | (G << 8) | B;
			}
		bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pix, 0, width, 0, 0, width, height);
		pix = null;
		long end = System.currentTimeMillis();
		Log.e("图像黑白化", "used time=" + (end - start));
		try {
			FileOutputStream fout = new FileOutputStream(
					Environment.getExternalStorageDirectory() + "/OCRimg2.jpeg");
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}

	/**
	 * 图片锐化（拉普拉斯变换）
	 * 
	 * @param bmp
	 * @return
	 */
	private Bitmap sharpenImageAmeliorate(Bitmap bmp) {
		long start = System.currentTimeMillis();
		// 拉普拉斯矩阵
		int[] laplacian = new int[] { -1, -1, -1, -1, 9, -1, -1, -1, -1 };
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		// Bitmap bitmap = Bitmap.createBitmap(width, height,
		// Bitmap.Config.RGB_565);
		Bitmap bitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		int pixR = 0;
		int pixG = 0;
		int pixB = 0;
		int pixColor = 0;
		int newR = 0;
		int newG = 0;
		int newB = 0;
		int idx = 0;
		float alpha = 0.3F;
		int[] pixels = new int[width * height];
		bmp.getPixels(pixels, 0, width, 0, 0, width, height);
		for (int i = 1, length = height - 1; i < length; i++) {
			for (int k = 1, len = width - 1; k < len; k++) {
				idx = 0;
				for (int m = -1; m <= 1; m++) {
					for (int n = -1; n <= 1; n++) {
						pixColor = pixels[(i + n) * width + k + m];
						pixR = Color.red(pixColor);
						pixG = Color.green(pixColor);
						pixB = Color.blue(pixColor);

						newR = newR + (int) (pixR * laplacian[idx] * alpha);
						newG = newG + (int) (pixG * laplacian[idx] * alpha);
						newB = newB + (int) (pixB * laplacian[idx] * alpha);
						idx++;
					}
				}

				newR = Math.min(255, Math.max(0, newR));
				newG = Math.min(255, Math.max(0, newG));
				newB = Math.min(255, Math.max(0, newB));

				pixels[i * width + k] = Color.argb(255, newR, newG, newB);
				newR = 0;
				newG = 0;
				newB = 0;
			}
		}
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		long end = System.currentTimeMillis();
		Log.e("图片锐化", "used time=" + (end - start));
		try {
			FileOutputStream fout = new FileOutputStream(
					Environment.getExternalStorageDirectory() + "/OCRimg2.jpeg");
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitmap;
	}

}