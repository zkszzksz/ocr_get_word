package com.nd.ocr_get_word.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

/**
 * @Describe: TODO  比分自定义Dialog
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/10 18:08
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class ShowCameraDialog extends Dialog {
    private static int default_width = 300; //默认宽度
    private static int default_height = 300;//默认高度
    private Context context;


    public ShowCameraDialog(Context context, int layout, int style) {
        super(context, style);
        this.context = context;
        setContentView(layout); //set content

        Window window = getWindow();//set window params
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.clearFlags(WindowManager.LayoutParams. ) ;// FLAG_DIM_BEHIND显示对话框时，后面的Activity不变暗，可选操作。

//        params.gravity = Gravity.TOP;
        params.y = -160;
        window.setAttributes(params);

        this.setCanceledOnTouchOutside(true); //点击区域外消失
    }

    public void setParamsBotton() {
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;

        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);

    }

    private float getDensity(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        return dm.density;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        ((Activity) context).onKeyDown(keyCode, event);
        return super.onKeyDown(keyCode, event);
    }
}