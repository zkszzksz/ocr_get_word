package com.nd.ocr_get_word.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import com.nd.ocr_get_word.util.ShowWindowManager;
import com.ocr.ocrtext.OCRLibrary;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @Description: 创建悬浮窗的逻辑都交给服务
 * @Package com.nd.ocr_get_word.service
 * @Date: 2014/11/19 14:05
 * @version: 1.0
 */
public class FloatWindowService extends Service {

	/**
	 * 用于在线程中创建或移除悬浮窗。
	 */
	private Handler handler = new Handler();

	/**
	 * 定时器，定时进行检测当前应该创建还是移除悬浮窗。
	 */
	private Timer timer;

	private RefreshTask refreshTask = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		OCRLibrary.init();
		// 开启定时器，每隔0.5秒刷新一次
		if (timer == null) {
			timer = new Timer();
			refreshTask = new RefreshTask();
			timer.scheduleAtFixedRate(refreshTask, 0, 2000);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		OCRLibrary.Relase();
		super.onDestroy();
		// Service被终止的同时也停止定时器继续运行
		try {
			refreshTask.cancel();
			refreshTask = null;
			timer.cancel();
			timer = null;
			
		} catch (Exception e) {
		}
	}

	class RefreshTask extends TimerTask {

		@Override
		public void run() {
			// 当前界面没有悬浮窗显示，则创建悬浮窗。
			try {
//				if (!ShowWindowManager.isWindowShowing()) { // isHome() &&
					handler.post(new Runnable() {
						@Override
						public void run() {
							ShowWindowManager
									.createSmallWindow(getApplicationContext());
						}
					});
//				} else if (ShowWindowManager.isWindowShowing()&&isHome()) { // isHome() &&
//					handler.post(new Runnable() {
//						@Override
//						public void run() {
//							if (ShowWindowManager.isBigViewShowing()) {
//								// ShowWindowManager.updateBigViewContent(getApplicationContext());
//							}
//						}
//					});
//				}
			} catch (Exception e) {
				 StackTraceElement[] stacks = e.getStackTrace();
     			StringBuffer sb = new StringBuffer();
     			for (int i = 0; i < stacks.length; i++) {
     				sb.append("class: ").append(stacks[i].getClassName())
     						.append("; method: ").append(stacks[i].getMethodName())
     						.append("; line: ").append(stacks[i].getLineNumber())
     						.append(";  Exception: ");
     			}
     			 System.out.printf("===============exceptionasd====:==" + sb.toString());
			}
		}
	}

	/**
	 * 判断当前界面是否是桌面
	 */
	private boolean isHome() {
		ActivityManager mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> rti = mActivityManager.getRunningTasks(1);
		return getHomes().contains(rti.get(0).topActivity.getPackageName());
	}

	/**
	 * 获得属于桌面的应用的应用包名称
	 * 
	 * @return 返回包含所有包名的字符串列表
	 */
	private List<String> getHomes() {
		List<String> names = new ArrayList<String>();
		PackageManager packageManager = this.getPackageManager();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		List<ResolveInfo> resolveInfo = packageManager.queryIntentActivities(
				intent, PackageManager.MATCH_DEFAULT_ONLY);
		for (ResolveInfo ri : resolveInfo) {
			names.add(ri.activityInfo.packageName);
		}
		return names;
	}
}