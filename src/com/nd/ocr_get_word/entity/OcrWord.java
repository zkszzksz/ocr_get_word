package com.nd.ocr_get_word.entity;

/**  
 * @Project: ocr_get_word
 * @Title: OcrWord.java
 * @Package com.nd.ocr_get_word.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2015-2-2 上午9:19:45
 * @Copyright: 2015 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class OcrWord {

	public int id;  
	public String word;
	public String explain;
	public boolean isEng = false;
	
}
