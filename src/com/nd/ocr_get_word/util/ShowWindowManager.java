package com.nd.ocr_get_word.util;

import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import com.nd.ocr_get_word.R;
import com.nd.ocr_get_word.activity.MainActivity;
import com.nd.ocr_get_word.entity.OcrWord;
import com.nd.ocr_get_word.view.FloatWindowBigView;
import com.nd.ocr_get_word.view.FloatWindowSmallView;
import com.nd.orc_get_word.db.HandleOcrWordDB;
import com.ocr.ocrtext.OCRLibrary;

/**
 * @author ZK
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @Description: 服务的管理器
 * @Package com.nd.ocr_get_word.util
 * @Date: 2014/11/19 14:05
 * @version: 1.0
 */
public class ShowWindowManager {

	/**
	 * 小悬浮窗View的实例
	 */
	private static FloatWindowSmallView smallWindow;

	/**
	 * 大悬浮窗View的实例
	 */
	private static FloatWindowBigView bigWindow;

	/**
	 * 小悬浮窗View的参数
	 */
	private static LayoutParams smallWindowParams;

	/**
	 * 大悬浮窗View的参数
	 */
	private static LayoutParams bigWindowParams;

	/**
	 * 用于控制在屏幕上添加或移除悬浮窗
	 */
	private static WindowManager mWindowManager;
	private static TextView topShowTV,fanyiTextView;

	private static Context mcontext = null;

	/**
	 * 创建一个小悬浮窗。初始位置为屏幕的右部中间位置。
	 * 
	 * @param context
	 *            必须为应用程序的Context.
	 */
	public static void createSmallWindow(Context context) {
		mcontext = context;
		if (null != smallWindow && smallWindow.isShown()) {
			return;
		}
		WindowManager windowManager = getWindowManager(context);
		int screenWidth = windowManager.getDefaultDisplay().getWidth();
		int screenHeight = windowManager.getDefaultDisplay().getHeight();
		if (smallWindow == null) {
			smallWindow = new FloatWindowSmallView(context);
			if (smallWindowParams == null) {
				smallWindowParams = new LayoutParams();
				smallWindowParams.type = WindowManager.LayoutParams.TYPE_PHONE;
				smallWindowParams.format = PixelFormat.RGBA_8888;
				smallWindowParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
						| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
				smallWindowParams.width = FloatWindowSmallView.viewWidth;
				smallWindowParams.height = FloatWindowSmallView.viewHeight;
				smallWindowParams.gravity = Gravity.LEFT | Gravity.TOP;
				smallWindowParams.x = screenWidth;
				smallWindowParams.y = screenHeight / 2;

			}
			smallWindow.setParams(smallWindowParams);
			windowManager.addView(smallWindow, smallWindowParams);
		}
	}

	/**
	 * 将小悬浮窗从屏幕上移除。
	 * 
	 * @param context
	 *            必须为应用程序的Context.
	 */
	public static void removeSmallWindow(Context context) {
		if (smallWindow != null) {
			WindowManager windowManager = getWindowManager(context);
			windowManager.removeView(smallWindow);
			smallWindow = null;
		}
	}

	/**
	 * 创建一个大悬浮窗。位置为屏幕正中间。
	 * 
	 * @param context
	 *            必须为应用程序的Context.
	 */
	public static void createBigWindow(Context context) {
		WindowManager windowManager = getWindowManager(context);
		if (bigWindow == null) {
			bigWindow = new FloatWindowBigView(context);
			if (bigWindowParams == null) {
				bigWindowParams = new LayoutParams();
				bigWindowParams.type = LayoutParams.TYPE_PHONE;
				bigWindowParams.format = PixelFormat.RGBA_8888;
				bigWindowParams.gravity = Gravity.CENTER;
				bigWindowParams.width = FloatWindowBigView.viewWidth;
				bigWindowParams.height = FloatWindowBigView.viewHeight;
			}
			windowManager.addView(bigWindow, bigWindowParams);
			ShowWindowManager.updateBigViewContent(context);
		}
	}

	/**
	 * 将大悬浮窗从屏幕上移除。
	 * 
	 * @param context
	 *            必须为应用程序的Context.
	 */
	public static void removeBigWindow(Context context) {
		if (bigWindow != null) {
			WindowManager windowManager = getWindowManager(context);
			windowManager.removeView(bigWindow);
			bigWindow = null;
		}
	}

	/**
	 * 更新大悬浮窗的TextView上的数据，
	 * 
	 * @param context
	 *            可传入应用程序上下文。
	 */
	public static void updateBigViewContent(final Context context) {
		if (bigWindow != null) {
			topShowTV = (TextView) bigWindow.findViewById(R.id.tv_top_text);
			fanyiTextView = (TextView) bigWindow.findViewById(R.id.tv_fanyi_text);
			topShowTV.setText("识别中...");
			new Thread(new Runnable() {
				@Override
				public void run() {
					Message msg = new Message();
					try {
						long start_time = System.currentTimeMillis();

						Bitmap resultBmp = BitmapFactory.decodeFile(Environment
								.getExternalStorageDirectory() + "/OCRimg2.jpeg");
						// imgFunction img = new imgFunction();
						// resultBmp = img.GetTargetBitmap(resultBmp);

						Bitmap allresultBmp = BitmapFactory
								.decodeFile(Environment
										.getExternalStorageDirectory()
										+ "/OCRimg3.jpeg");
						if (null == resultBmp) {
							msg.obj = new String[] { "木有图片", ""};
						} else if (MainActivity.isChinese.isChecked()) {
							String oneword = OCRLibrary.OCRTxt(resultBmp, 1); // 1是中文，2是英语
							String allword = OCRLibrary.OCRTxt(allresultBmp, 1); // 1是中文，2是英语
							msg.obj = new String[] { "" + oneword, "" + allword };
						} else {
							msg.obj =new String[] {""+ OCRLibrary.OCRTxt(resultBmp, 2),""}; // 1是中文，2是英语
						}
						long end_time = System.currentTimeMillis();
						msg.arg1 = (int) (end_time - start_time);
						msg.what = 1000;
					} catch (Exception e) {
						StackTraceElement[] stacks = e.getStackTrace();
						StringBuffer sb = new StringBuffer();
						for (int i = 0; i < stacks.length; i++) {
							sb.append("class: ")
									.append(stacks[i].getClassName())
									.append("; method: ")
									.append(stacks[i].getMethodName())
									.append("; line: ")
									.append(stacks[i].getLineNumber())
									.append(";  Exception: ");
						}
						System.out.println("==========handleMessage========="+e.toString());
						msg.obj = new String[]{"",""};
						msg.what = 1001;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	private static Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1000:
				if (null != msg.obj) {
					String[] obj = (String[]) msg.obj;
					OcrWord ocr = setOCRresult(""+obj[0],""+obj[1]);
					if(null!=ocr){
						if(ocr.isEng){
							topShowTV.setText("英文： "+ocr.word);
							fanyiTextView.setText("中文: "+ocr.explain);
						}else{
							topShowTV.setText("中文： "+ocr.word);
							fanyiTextView.setText("英文: "+ocr.explain);
						}
					}else{
						if(Utils.strIsEnglish(""+obj[0])){
							topShowTV.setText("英文： "+obj[1]);
						}else{
							topShowTV.setText("中文： "+obj[1]);
						}
						fanyiTextView.setText("暂无");
					}
				} else {
					topShowTV.setText(" " + "识别失败");
					fanyiTextView.setText("暂无");
				}
				break;
				
			case 1001:
				topShowTV.setText(" " + "识别失败");
				fanyiTextView.setText("暂无");
				break;
			default:
				break;
			}
		}
	};

	/**
	 * 
	 * @Title: setOCRresult
	 * @Description: TODO
	 * @param @param str1 光标下的字
	 * @param @param str2 横排的字
	 * @param @return
	 * @return OcrWord
	 */
	public static OcrWord setOCRresult(String str1, String str2) {
		HandleOcrWordDB db = new HandleOcrWordDB(mcontext);
		OcrWord ocrWord = null;
		int length = str2.length();
		if (length == 0) {
			return null;
		}
		if (Utils.strIsEnglish(str1)) {
			return db.getOcrWord(str2.toLowerCase());
		}
		int mid = 0;
		if ("".equals(str1)) {
			mid = length / 2;
		} else if (str1.length() == 1) {
			boolean ishandle = false;
			for (int i = 0; i < str2.length(); i++) {
				if (str1.equals("" + str2.charAt(i))) {
					mid = i;
					ishandle = true;
					break;
				}
			}
			if (!ishandle) {
				mid = length / 2;
			}
		}
		String mstr = "" + str2.charAt(mid);
		String select = mstr;
		boolean isresult = false;
		if (str2.length() == 1) {
			return db.getOcrWord(mstr);
		} else {
			for (int i = mid + 1; i < str2.length(); i++) {
				select = select + str2.charAt(i);
				if (null != db.getOcrWord(select)) {
					isresult = true;
					return db.getOcrWord(select);
				}
			}
			select = mstr;
			if (mid - 1 >= 0) {
				for (int i = mid - 1; i >= 0; i--) {
					select = str2.charAt(i) + select;
					for (int j = mid + 1; j < str2.length(); j++) {
						select = select + str2.charAt(j);
						if (null != db.getOcrWord(select)) {
							isresult = true;
							return db.getOcrWord(select);
						}
					}
				}
			}
			select = mstr;
			if (mid - 1 >= 0) {
				for (int i = mid - 1; i >= 0; i--) {
					select = str2.charAt(i) + select;
					if (null != db.getOcrWord(select)) {
						isresult = true;
						return db.getOcrWord(select);
					}
				}
			}
		}
		if (!isresult) {
			return db.getOcrWord(mstr);
		}
		return ocrWord;
	}

	/**
	 * 是否有悬浮窗(包括小悬浮窗和大悬浮窗)显示在屏幕上。
	 * 
	 * @return 有悬浮窗显示在桌面上返回true，没有的话返回false。
	 */
	public static boolean isWindowShowing() {
		return null != smallWindow || null != bigWindow;
	}

	/**
	 * 是否有大悬浮窗
	 * 
	 * @return
	 */
	public static boolean isBigViewShowing() {
		return bigWindow != null;
	}

	/**
	 * 如果WindowManager还未创建，则创建一个新的WindowManager返回。否则返回当前已创建的WindowManager。
	 * 
	 * @param context
	 *            必须为应用程序的Context.
	 * @return WindowManager的实例，用于控制在屏幕上添加或移除悬浮窗。
	 */
	private static WindowManager getWindowManager(Context context) {
		if (mWindowManager == null) {
			mWindowManager = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
		}
		return mWindowManager;
	}

	/**
	 * 剪贴板内容
	 * 
	 * @param context
	 *            可传入应用程序上下文。
	 */
	@SuppressWarnings("deprecation")
	public static String getUsedPercentValue(Context context) {
		return ((ClipboardManager) context
				.getSystemService(Context.CLIPBOARD_SERVICE)).getText()
				.toString(); // 这个是剪贴板内容
	}

}