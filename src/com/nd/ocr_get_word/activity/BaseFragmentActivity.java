package com.nd.ocr_get_word.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.nd.ocr_get_word.R;
import com.nd.ocr_get_word.util.Utils;
import com.nd.ocr_get_word.view.ShowCameraDialog;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO
 * @Package com.nd.ocr_get_word.activity
 * @Date: 2014/11/21 10:03
 * @version: 1.0
 */
public class BaseFragmentActivity extends FragmentActivity {
    protected Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    public static final int CAMERA = 1102;// 手机拍摄
    public static final int GALLEY = 1101;// 手机相册
    public static final int PHOTO_REQUEST_CUT = 1104;// 裁剪之后
    protected static final int RESULT_IMG = 1108;// 返回不裁剪
    protected static final int RESULT_CUT_IMG = 1109;// 返回裁剪
    private Handler handler;
    private boolean isCut = false;
    private int cutW = 200, cutH = 200;
    private Uri cameraUri;
    private boolean isScale = true;

    /**
     * TODO 以下为调用系统相机代码，目前bug：传true切图，照相的话问题。
     * 返回值为图片路径，ImageView.setImageBitmap(BitmapFactory.decodeFile(""+msg.obj));
     * TODO 如果需要压缩： Utils.compressImage(""+msg.obj);方法即可
     *
     * @param handler 需要hanlder接收数据:RESULT_IMG/RESULT_CUT_IMG，
     * @param isCut   是否裁剪
     * @param cutW    isCut为真才有效，裁剪宽度
     * @param cutH    裁剪高度
     * @param isScale 是否固定裁剪的比例,true表固定比例，传参cutW和cutH才有效 ;false 是自由裁剪
     */
    protected void showCameraDialog(Handler handler, final boolean isCut,
                                    int cutW, int cutH, boolean isScale) {
        this.handler = handler;
        this.isCut = isCut;
        this.cutW = cutW;
        this.cutH = cutH;
        this.isScale = isScale;
        String sdState = Environment.getExternalStorageState();
        if (!sdState.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            Toast.makeText(mContext, "sd卡不存在",3000).show();
            return;
        }
        final ShowCameraDialog dialog = new ShowCameraDialog(mContext,
                R.layout.select_camera_dialog, R.style.sty_dialog_camera);
        dialog.setParamsBotton();
        dialog.show();
        dialog.findViewById(R.id.select_camera_dialog_camera)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent camera = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraUri = Utils.createImagePathUri(mContext);
                        camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                        startActivityForResult(camera, CAMERA);
                    }
                });
        dialog.findViewById(R.id.select_camera_dialog_album)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent picture = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(picture, GALLEY);
                    }
                });
        dialog.findViewById(R.id.select_camera_dialog_back).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.sty_dialog_camera);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String picpath = "";
        Message msg = new Message();

        if (requestCode == CAMERA && resultCode == Activity.RESULT_OK
                && null == data) {
            if (isCut) {
                startPhotoZoom(cameraUri);
            } else {
                msg.what = RESULT_IMG;
                msg.obj = Utils.uriToPath(mContext, cameraUri);
                handler.sendMessage(msg);
            }
        } else if (requestCode == GALLEY && resultCode == Activity.RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumns = {MediaStore.Images.Media.DATA};
            Cursor c = mContext.getContentResolver().query(selectedImage,
                    filePathColumns, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePathColumns[0]);
            String picturePath = c.getString(columnIndex);
            picpath = picturePath;
            c.close();

            if (isCut) {
                // startPhotoZoom(Utils.pathToUri(mContext,picpath));
                startPhotoZoom(selectedImage);
            } else {
                msg.what = RESULT_IMG;
                msg.obj = picpath;
                handler.sendMessage(msg);
            }
        } else if (requestCode == PHOTO_REQUEST_CUT
                && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap bmp = data.getExtras().getParcelable("data");
            msg.what = RESULT_CUT_IMG;
            msg.obj = bmp;
            handler.sendMessage(msg);
        }
    }

    private void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // crop为true是设置在开启的intent中设置显示的view可以剪裁
        intent.putExtra("crop", "true");
        if (isScale) { // 目前bug，拍照不能自由比例
            // aspectX aspectY 是宽高的比例
            intent.putExtra("aspectX", cutW);
            intent.putExtra("aspectY", cutH);
        }
        // outputX,outputY 是剪裁图片的宽高,不能设置过大
        intent.putExtra("outputX", cutW);
        intent.putExtra("outputY", cutH);
//        intent.putExtra("scale", true);  //	是否保留比例

        intent.putExtra("return-data", true);
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }
}
