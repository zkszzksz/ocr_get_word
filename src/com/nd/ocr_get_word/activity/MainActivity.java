package com.nd.ocr_get_word.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;

import com.nd.ocr_get_word.R;
import com.nd.ocr_get_word.entity.OcrWord;
import com.nd.ocr_get_word.service.FloatWindowService;
import com.nd.ocr_get_word.util.ShowWindowManager;
import com.nd.ocr_get_word.util.Utils;
import com.nd.ocr_get_word.view.FloatWindowSmallView;
import com.nd.orc_get_word.db.HandleOcrWordDB;
import com.nd.orc_get_word.db.OcrWordsDatabaseDBhelper;
import com.ocr.ocrtext.OCRLibrary;

/**
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @Description: 主界面，用于启动悬浮窗
 * @author: 张凯
 * @Package com.nd.ocr_get_word.activity
 * @Date: 2014/11/19 14:05
 * @version: 1.0
 */
public class MainActivity extends BaseFragmentActivity implements
		View.OnClickListener {

	private Context mContext;
	private CheckBox isShowWindow;
	private Bitmap resultBmp;
	private TextView showTv;
	public static CheckBox isChinese;
	private ImageView showImg;

	private TextView tbTextView = null;
	private TextView selectTextView = null;

	private HandleOcrWordDB db = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_main);
		OCRLibrary.init();
		mContext = this;
		db = new HandleOcrWordDB(mContext);
		initData();
	}

	private void initData() {
		isShowWindow = (CheckBox) findViewById(R.id.cb_start_float_window);
		isShowWindow
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							Intent intent = new Intent(mContext,
									FloatWindowService.class);
							startService(intent);
						} else {
							// 点击关闭悬浮窗的时候，移除所有悬浮窗，并停止Service
							ShowWindowManager.removeBigWindow(mContext);
							ShowWindowManager.removeSmallWindow(mContext);
							Intent intent = new Intent(mContext,
									FloatWindowService.class);
							stopService(intent);
						}
					}
				});
		isChinese = (CheckBox) findViewById(R.id.btn_set_change);
		findViewById(R.id.tv_show_check_img).setOnClickListener(this);
		showTv = ((TextView) findViewById(R.id.tv_show_translate));
		// showTv.setText(""+getApplicationContext().getFilesDir().getAbsolutePath());
		showTv.setOnClickListener(this);
		showImg = ((ImageView) findViewById(R.id.iv_show));
		showImg.setOnClickListener(this);
		tbTextView = (TextView) findViewById(R.id.tb_words);
		tbTextView.setOnClickListener(this);
		selectTextView = (TextView) findViewById(R.id.tb_select);
		selectTextView.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (ShowWindowManager.isWindowShowing()) {
			isShowWindow.setChecked(true);
		} else {
			isShowWindow.setChecked(false);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.tb_words:
			tbWords();
			break;

		case R.id.tb_select:
			OcrWord ocr = setOCRresult("p", "person");
			if(null!=ocr){
				System.out.println(""+ocr.word);
			}
			break;

		case R.id.tv_show_check_img:
			showCameraDialog(handler, true, 300, 300, true);
			break;
		case R.id.iv_show:
			Bitmap bmp = FloatWindowSmallView.getScreenBmp();
			if (null != bmp) {
				showImg.setImageBitmap(bmp);
			} else {
				Toast.makeText(mContext, "图片为空", 3000).show();
			}
			break;
		case R.id.tv_show_translate:
			if (null == resultBmp) {
				Toast.makeText(mContext, "请选择图片", 300).show();
			} else if (showTv.getText().toString().equals("识别中：")) {
				Toast.makeText(mContext, "正在识别，请稍后", 300).show();
			} else {
				showTv.setText("识别中：");
				new Thread(new Runnable() {
					@Override
					public void run() {
						Message msg = new Message();
						try {

							long start_time = System.currentTimeMillis();

							if (isChinese.isChecked())
								msg.obj = OCRLibrary.OCRTxt(resultBmp, 1); // 1是中文，2是英语
							else
								msg.obj = OCRLibrary.OCRTxt(resultBmp, 2); // 1是中文，2是英语

							long end_time = System.currentTimeMillis();
							msg.arg1 = (int) (end_time - start_time);
						} catch (Exception e) {
							System.out.printf("exception:" + e.toString());
							msg.obj = e.toString();
						}
						msg.what = 1;
						handler.sendMessage(msg);
					}
				}).start();
			}
			break;
		default:
			break;
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case RESULT_IMG:
				resultBmp = BitmapFactory.decodeFile(msg.obj.toString());
				showImg.setImageBitmap(resultBmp);
				break;
			case RESULT_CUT_IMG:
				resultBmp = (Bitmap) msg.obj;
				showImg.setImageBitmap(resultBmp);
				break;
			case 1:
				if (null != msg.obj)
					showTv.setText("有图后点击翻译结果：" + msg.obj);
				else
					showTv.setText("有图后点击翻译结果：" + "识别失败");
				Toast.makeText(getApplicationContext(),
						"本次识别用时：" + (msg.arg1 / 1000) + "秒", Toast.LENGTH_LONG)
						.show();
				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onDestroy() {
		OCRLibrary.Relase();
		super.onDestroy();
	}

	@Override
	public void onConfigurationChanged(Configuration config) {
		super.onConfigurationChanged(config);
	}

	private void tbWords() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					OcrWordsDatabaseDBhelper d = OcrWordsDatabaseDBhelper
							.getInstance(mContext);
					msg.what = 1000;
				} catch (Exception e) {
					msg.what = 1001;
				}
				drhandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler drhandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1000:
				Toast.makeText(mContext, "导入成功", 0).show();
				break;
			case 1001:
				Toast.makeText(mContext, "导入失败", 0).show();
				break;

			default:
				break;
			}
		};
	};

	private OcrWord setOCRresult(String str1, String str2) {
		OcrWord ocrWord = null;
		int length = str2.length();
		if (length == 0) {
			return null;
		}
		if (Utils.strIsEnglish(str1)) {
			return db.getOcrWord(str2);
		}
		int mid = 0;
		if ("".equals(str1)) {
			mid = length / 2;
		} else if (str1.length() == 1) {
			boolean ishandle = false;
			for (int i = 0; i < str2.length(); i++) {
				if (str1.equals("" + str2.charAt(i))) {
					mid = i;
					ishandle = true;
					break;
				}
			}
			if (!ishandle) {
				mid = length / 2;
			}
		}
		String mstr = "" + str2.charAt(mid);
		String select = mstr;
		boolean isresult = false;
		if (str2.length() == 1) {
			return db.getOcrWord(mstr);
		} else {
			for (int i = mid + 1; i < str2.length(); i++) {
				select = select + str2.charAt(i);
				if (null != db.getOcrWord(select)) {
					isresult = true;
					return db.getOcrWord(select);
				}
			}
			select = mstr;
			if(mid-1>=0){
				for (int i = mid-1; i >=0; i--) {
					select = str2.charAt(i)+select;
					for (int j = mid + 1; j < str2.length(); j++) {
						select = select+str2.charAt(j);
						if (null != db.getOcrWord(select)) {
							isresult = true;
							return db.getOcrWord(select);
						}
					}
				}
			}
			select = mstr;
			if(mid-1>=0){
				for (int i = mid-1; i >=0; i--) {
					select = str2.charAt(i)+select;
					if (null != db.getOcrWord(select)) {
						isresult = true;
						return db.getOcrWord(select);
					}
				}
			}
		}
		if(!isresult){
			return db.getOcrWord(mstr);
		}
		return ocrWord;
	}

}
